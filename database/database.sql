-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema collegeplanner
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `collegeplanner` ;

-- -----------------------------------------------------
-- Schema collegeplanner
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `collegeplanner` DEFAULT CHARACTER SET utf8 ;
USE `collegeplanner` ;

-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user` ;

CREATE TABLE IF NOT EXISTS `user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `gpa` DECIMAL(3,2) NOT NULL DEFAULT 0.00,
  `firstname` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `registered_date` DATETIME NOT NULL,
  `key` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `year`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `year` ;

CREATE TABLE IF NOT EXISTS `year` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `gpa` DECIMAL(3,2) NOT NULL DEFAULT 0.00,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_year_user_idx` (`user_id` ASC),
  CONSTRAINT `fk_year_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `semester`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `semester` ;

CREATE TABLE IF NOT EXISTS `semester` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `year_id` INT NOT NULL,
  `gpa` DECIMAL(3,2) NOT NULL DEFAULT 0.00,
  `start` DATE NOT NULL,
  `end` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_semester_year1_idx` (`year_id` ASC),
  CONSTRAINT `fk_semester_year1`
    FOREIGN KEY (`year_id`)
    REFERENCES `year` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `class`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `class` ;

CREATE TABLE IF NOT EXISTS `class` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `semester_id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `detail` TEXT NULL,
  `color` CHAR(6) NOT NULL,
  `gpa` DECIMAL(3,2) NOT NULL DEFAULT 0.00,
  `credit` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_class_semester1_idx` (`semester_id` ASC),
  CONSTRAINT `fk_class_semester1`
    FOREIGN KEY (`semester_id`)
    REFERENCES `semester` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `time`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `time` ;

CREATE TABLE IF NOT EXISTS `time` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `class_id` INT NOT NULL,
  `days` INT(1) NOT NULL,
  `location` VARCHAR(100) NULL,
  `start` TIME NULL,
  `end` TIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_time_class1_idx` (`class_id` ASC),
  CONSTRAINT `fk_time_class1`
    FOREIGN KEY (`class_id`)
    REFERENCES `class` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `task`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `task` ;

CREATE TABLE IF NOT EXISTS `task` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `class_id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `deadline` DATETIME NOT NULL,
  `detail` VARCHAR(100) NULL,
  `img` VARCHAR(100) NULL,
  INDEX `fk_task_class1_idx` (`class_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_task_class1`
    FOREIGN KEY (`class_id`)
    REFERENCES `class` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `score`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `score` ;

CREATE TABLE IF NOT EXISTS `score` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `class_id` INT NOT NULL,
  `date` DATE NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `detail` TEXT NULL,
  `score` INT(3) NOT NULL,
  `weight` INT(3) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_score_class1_idx` (`class_id` ASC),
  CONSTRAINT `fk_score_class1`
    FOREIGN KEY (`class_id`)
    REFERENCES `class` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `user`
-- -----------------------------------------------------
START TRANSACTION;
USE `collegeplanner`;
INSERT INTO `user` (`id`, `email`, `password`, `gpa`, `firstname`, `lastname`, `registered_date`, `key`) VALUES (1, 'demo@demo.com', 'demo', DEFAULT, 'demo', 'demo', '2012-02-01', '12345');

COMMIT;


-- -----------------------------------------------------
-- Data for table `year`
-- -----------------------------------------------------
START TRANSACTION;
USE `collegeplanner`;
INSERT INTO `year` (`id`, `user_id`, `gpa`, `name`) VALUES (1, 1, DEFAULT, 'Year Demo');

COMMIT;


-- -----------------------------------------------------
-- Data for table `semester`
-- -----------------------------------------------------
START TRANSACTION;
USE `collegeplanner`;
INSERT INTO `semester` (`id`, `year_id`, `gpa`, `start`, `end`) VALUES (2, 1, DEFAULT, '2018-01-03', '2019-01-03');
INSERT INTO `semester` (`id`, `year_id`, `gpa`, `start`, `end`) VALUES (1, 1, DEFAULT, '2013-01-03', '2014-01-03');

COMMIT;


-- -----------------------------------------------------
-- Data for table `class`
-- -----------------------------------------------------
START TRANSACTION;
USE `collegeplanner`;
INSERT INTO `class` (`id`, `semester_id`, `name`, `detail`, `color`, `gpa`, `credit`) VALUES (1, 1, 'Class Demo', 'Demo Detail', '000000', DEFAULT, 2);
INSERT INTO `class` (`id`, `semester_id`, `name`, `detail`, `color`, `gpa`, `credit`) VALUES (2, 2, 'Class Demo 2', 'Demo Detail', 'FFFFFF', DEFAULT, 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `time`
-- -----------------------------------------------------
START TRANSACTION;
USE `collegeplanner`;
INSERT INTO `time` (`id`, `class_id`, `days`, `location`, `start`, `end`) VALUES (1, 1, 1, 'SBY', '08.00', '11.00');

COMMIT;


-- -----------------------------------------------------
-- Data for table `task`
-- -----------------------------------------------------
START TRANSACTION;
USE `collegeplanner`;
INSERT INTO `task` (`id`, `class_id`, `name`, `deadline`, `detail`, `img`) VALUES (1, 1, 'Task Demo', '2018-06-01', NULL, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `score`
-- -----------------------------------------------------
START TRANSACTION;
USE `collegeplanner`;
INSERT INTO `score` (`id`, `class_id`, `date`, `name`, `detail`, `score`, `weight`) VALUES (1, 1, '2018-5-8', 'NILAIKU', NULL, 10, 10);

COMMIT;

