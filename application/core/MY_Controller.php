<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//https://stackoverflow.com/a/38866812
require APPPATH . 'core/REST_Controller.php';

class MY_Controller extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model("User_model", "user_model");
    }

    public function get_id_from_api_key($key){
        $id = $this->user_model->get_id_from_key($key);
        if($id){
            return $id;
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Token Invalid' 
            ], REST_Controller::HTTP_UNAUTHORIZED); //401
            return false;
        }
    }
}
