<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes_model extends CI_Model {
    
    public function get_classes_user($user_id){
        $query = $this->db
        ->select("class.id, class.semester_id, class.name, class.detail, class.color, class.gpa, class.credit")
        ->from("class")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
		->where("year.user_id", $user_id)
        ->get();
    
        return $query->result();
    }

    public function get_classes_semester($user_id, $semester_id){
        $query = $this->db
        ->select("class.id, class.semester_id, class.name, class.detail, class.color, class.gpa, class.credit")
        ->from("class")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->where("semester.id", $semester_id)
        ->get();
        return $query->result();
    }

    public function get_classes_year($user_id, $year_id){
        $query = $this->db
        ->select("class.id, class.semester_id, class.name, class.detail, class.color, class.gpa, class.credit")
        ->from("class")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->where("year.id", $year_id)
        ->get();
        return $query->result();
    }

    public function get_class($user_id, $class_id){
        $queryClass = $this->db
        ->select("class.id, class.semester_id, class.name, class.detail, class.color, class.gpa, class.credit")
        ->from("class")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->where("class.id", $class_id)
        ->get()
		->row();
		
		$return = json_decode(json_encode($queryClass), true);

		$queryTime = $this->db
		->select("time.id, time.days, time.location, time.class_id, class.name as class_name, class.semester_id, semester.year_id, class.color as class_color")
		->select("DATE_FORMAT(time.start, '" . $this->config->item('mysql_time_format') . "') as start", false)
		->select("DATE_FORMAT(time.end, '" . $this->config->item('mysql_time_format') . "') as end", false)
		->from("class")
		->join("semester", "semester.id = class.semester_id", "left")
		->join('year', 'year.id = semester.year_id', "left")
		->join('time', 'time.class_id = class.id')
		->where('class.id', $class_id)
		->order_by('time.days ASC, time.start ASC')
		->where("year.user_id", $user_id)
		->get()
		->result();

		$return['time'] = (array)$queryTime;

		$queryScore = $this->db
		->select("score.id, score.name, score.detail, score.score, score.weight, score.class_id, class.name as class_name, class.semester_id, semester.year_id, class.color as class_color")
		->select("DATE_FORMAT(score.date, '" . $this->config->item('mysql_date_format') . "') as date", false)
		->from("class")
		->join("semester", "semester.id = class.semester_id", "left")
		->join('year', 'year.id = semester.year_id', "left")
		->join('score', 'score.class_id = class.id')
		->where('class.id', $class_id)
		->where("year.user_id", $user_id)
		->order_by('score.date ASC')
		->get()
		->result();

		$return['score'] = (array)$queryScore;

		$queryTask= $this->db
		->select("task.id, task.name, task.detail, task.img, task.class_id, class.name as class_name, class.semester_id, semester.year_id, class.color as class_color")
		->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_date_format') . "') as deadline_date", false)
		->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_time_format') . "') as deadline_time", false)
		->from("class")
		->join("semester", "semester.id = class.semester_id", "left")
		->join('year', 'year.id = semester.year_id', "left")
		->join('task', 'task.class_id = class.id')
		->where('class.id', $class_id)
		->where("year.user_id", $user_id)
		->order_by('task.deadline ASC')
		->get()
		->result();

		$return['task'] = (array)$queryTask;
		
        return $return;
    }

    public function add_class($user_id, $semester_id, $name, $color, $credit, $detail = null){
        $query = $this->db
        ->select("count(*) as count")
        ->from("semester")
        ->join('year', 'year.id = semester.year_id')
        ->where("year.user_id", $user_id)
        ->where("semester.id", $semester_id)
        ->get();
        if(intval($query->row()->count) >= 1){
            $data = array(
                'semester_id' => $semester_id,
                'name' => $name,
                'detail' => $detail,
                'color' => $color,
                'gpa' => 0.00,
                'credit' => $credit
            );
            $data = array_filter($data); //remove null array
            return $this->db->insert('class', $data);
        }
        else return 0;
        
    }

    
}