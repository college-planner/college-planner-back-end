<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Year_model extends CI_Model {
	
	public function get_year_header($user_id){
		$query = $this->db->
		select("user.gpa, ifnull(sum(class.credit), 0) as credit")->
		from("user")->
		join("year", "year.user_id = user.id", "left")->
		join("semester", "semester.year_id = year.id", "left")->
		join("class", "class.semester_id = semester.id", "left")->
		where("user_id", $user_id)->
		group_by("class.credit")->
		get();
        return $query->row();
	}
    
    public function get_years($user_id){
        $query = $this->db->where("user_id", $user_id)->select("id, gpa, name")->get("year");
        return $query->result();
    }

    public function get_year($year_id){
        $query = $this->db->where("id", $year_id)->select("id, gpa, name")->get("year");
        return $query->row();
    }

    public function update_year($year_id, $gpa, $name){
        $data = array(
            'gpa' => $gpa,
            'name' => $name
        );
        return $this->db->where("id", $year_id)->update("year", $data);
    }

    public function add_year($user_id, $name){
        $data = array(
            'user_id' => $user_id,
            'gpa' => 0.00,
            'name' => $name
        );
        $query = $this->db->insert('year', $data);
        return $query;
    }
}