<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Semester_model extends CI_Model {
    
    public function get_semesters_user($user_id){
        $query = $this->db
        ->select("semester.id, semester.gpa, semester.start, semester.end, year.id as year_id")
        ->select("DATE_FORMAT(semester.start, '" . $this->config->item('mysql_date_format') . "') as start", false)
        ->select("DATE_FORMAT(semester.end, '" . $this->config->item('mysql_date_format') . "') as end", false)
        ->from("semester")
        ->join('year', 'year.id = semester.year_id', 'left')
        ->where("year.user_id", $user_id)
        ->get();
        return $query->result();
    }

    public function get_semesters_year($user_id, $year_id){
        $query = $this->db
        ->select("semester.id, semester.gpa, year.id as year_id")
        ->select("DATE_FORMAT(semester.start, '" . $this->config->item('mysql_date_format') . "') as start", false)
        ->select("DATE_FORMAT(semester.end, '" . $this->config->item('mysql_date_format') . "') as end", false)
        ->from("semester")
        ->join('year', 'year.id = semester.year_id', 'left')
        ->where("year.user_id", $user_id)
        ->where("year.id", $year_id)
        ->get();
        return $query->result();
    }

    public function get_semester($user_id, $semester_id){
        $query = $this->db
        ->select("semester.id, semester.gpa, year.id as year_id")
        ->select("DATE_FORMAT(semester.start, '" . $this->config->item('mysql_date_format') . "') as start", false)
        ->select("DATE_FORMAT(semester.end, '" . $this->config->item('mysql_date_format') . "') as end", false)
        ->from("semester")
        ->join('year', 'year.id = semester.year_id', 'left')
        ->where("year.user_id", $user_id)
        ->where("semester.id", $semester_id)
        ->get();
        return $query->row();
    }

    public function update_semester($user_id, $year_id, $semester_id, $gpa, $start, $end){
        $query = $this->db
        ->select("count(*) as count")
        ->from("year")
        ->where("year.user_id", $user_id)
        ->where("year.id", $year_id)
        ->get();
        if(intval($query->row()->count) >= 1){
            $data = array(
                'gpa' => $gpa
            );
            $this->db->set('`start`', "STR_TO_DATE('".$start."', '" . $this->config->item('mysql_date_format') . "')", FALSE);
            $this->db->set('`end`', "STR_TO_DATE('".$end."', '" . $this->config->item('mysql_date_format') . "')", FALSE);
            $data = array_filter($data); //remove null array
            return $this->db->where("id", $semester_id)->update("semester", $data);
        }
        else return 0;
        
    }

    public function add_semester($user_id, $year_id, $start, $end){
        $query = $this->db
        ->select("count(*) as count")
        ->from("year")
        ->where("year.user_id", $user_id)
        ->where("year.id", $year_id)
        ->get();
        if(intval($query->row()->count) >= 1){
            $data = array(
                'year_id' => $year_id,
                'gpa' => 0.00
            );
            $this->db->set('`start`', "STR_TO_DATE('".$start."', '" . $this->config->item('mysql_date_format') . "')", FALSE);
            $this->db->set('`end`', "STR_TO_DATE('".$end."', '" . $this->config->item('mysql_date_format') . "')", FALSE);
            return $this->db->insert('semester', $data);
        }
        else return 0;
        
    }

    
}