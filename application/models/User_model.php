<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function get_id_from_key($key){
        $query = $this->db->where("key", $key)->select("id")->get("user");
        $row = $query->row();
        if(isset($row->id)){
            return $row->id;
        }
        else return false;
    }
    
    public function email_exist($email){
        if($this->db->where('email', $email)->from('user')->count_all_results() >= 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function register($email, $password, $firstname, $lastname, $key){
        $data = array(
            'email' => $email,
            'password' => $password,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'registered_date' => date('Y-m-d H:i:s'),
            'key' => $key,
        );
        return $this->db->insert("user", $data);
    }

    public function login($email, $password){
        $data = array(
            'email' => $email,
            'password' => $password,
        );
        $query = $this->db->where($data)->select("key")->get("user");
        return $query->row();
    }

    public function user($user_id){
        $query = $this->db->where("id", $user_id)->select("email, gpa, firstname, lastname")->get("user");
        return $query->row();
    }
	
	public function update_gpa($id, $gpa){
        $data = array(
            'gpa' => $gpa
        );
        return $this->db->where("id", $id)->update("user", $data);
    }

    public function update_email($id, $email){
        $data = array(
            'email' => $email
        );
        return $this->db->where("id", $id)->update("user", $data);
    }
	
	public function update_password($id, $password){
        $data = array(
            'password' => $password
        );
        return $this->db->where("id", $id)->update("user", $data);
    }
	
	public function update_name($id, $firstname, $lastname){
        $data = array(
            'firstname' => $firstname,
			'lastname' => $lastname
        );
        return $this->db->where("id", $id)->update("user", $data);
    }
	
	public function check_password($id, $password){
		$query = $this->db->select("count(*) as count")->
		where("id", $id)->
		where("password", $password)->
		get("user");
		if($query->row()->count == 1){
			return true;
		}
		else{
			return false;
		}
	}
}