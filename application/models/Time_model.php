<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time_model extends CI_Model {
    
    /*public function get_task_user($user_id){
        $query = $this->db
        ->select("task.id, task.class_id, task.name, task.detail, task.img")
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_date_format') . "') as deadline_date", false)
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_time_format') . "') as deadline_time", false)
        ->from("task")
        ->join("class", "class.id = task.class_id", "left")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->get();
        
        return $query->result();
    }

    public function get_task_class($user_id, $class_id){
        $query = $this->db
        ->select("task.id, task.class_id, task.name, task.detail, task.img")
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_date_format') . "') as deadline_date", false)
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_time_format') . "') as deadline_time", false)
        ->from("task")
        ->join("class", "class.id = task.class_id", "left")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->where("class.id", $class_id)
        ->get();
        return $query->result();
    }

    public function get_task_semester($user_id, $semester_id){
        $query = $this->db
        ->select("task.id, task.class_id, task.name, task.detail, task.img")
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_date_format') . "') as deadline_date", false)
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_time_format') . "') as deadline_time", false)
        ->from("task")
        ->join("class", "class.id = task.class_id", "left")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->where("semester.id", $semester_id)
        ->get();
        return $query->result();
    }

    public function get_task_year($user_id, $year_id){
        $query = $this->db
        ->select("task.id, task.class_id, task.name, task.detail, task.img")
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_date_format') . "') as deadline_date", false)
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_time_format') . "') as deadline_time", false)
        ->from("task")
        ->join("class", "class.id = task.class_id", "left")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->where("year.id", $year_id)
        ->get();
        return $query->result();
    }

    public function get_task($user_id, $task_id){
        $query = $this->db
        ->select("task.id, task.class_id, task.name, task.detail, task.img")
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_date_format') . "') as deadline_date", false)
        ->select("DATE_FORMAT(task.deadline, '" . $this->config->item('mysql_time_format') . "') as deadline_time", false)
        ->from("task")
        ->join("class", "class.id = task.class_id", "left")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->where("task.id", $task_id)
        ->get();
        return $query->row();
    }*/

    public function add_time($user_id, $class_id, $days, $location, $start, $end){
        $query = $this->db
        ->select("count(*) as count")
        ->from("class")
        ->join("semester", "semester.id = class.semester_id", "left")
        ->join('year', 'year.id = semester.year_id', "left")
        ->where("year.user_id", $user_id)
        ->where("class.id", $class_id)
        ->get();
        if(intval($query->row()->count) >= 1){
            $data = array(
                'class_id' => $class_id,
                'days' => $days,
                'location' => $location,
            );
            $this->db->set('`start`', "STR_TO_DATE('".$start."', '" . $this->config->item('mysql_time_format') . "')", FALSE);
            $this->db->set('`end`', "STR_TO_DATE('".$end."', '" . $this->config->item('mysql_time_format') . "')", FALSE);
            return $this->db->insert('time', $data);
        }
        else return 0;
        
    }

    
}