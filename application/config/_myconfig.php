<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['mysql_date_format'] = '%d-%m-%Y';
$config['mysql_time_format'] = '%H:%i';
$config['mysql_date_time_format'] = '%d-%m-%Y %H:%i';
