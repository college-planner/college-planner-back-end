<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("Time_model", "time_model");
    }

    /*public function alltask_user_get(){
        $key = $this->get("key");
        if($key !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task_user($id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK); //200
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get tasks' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function alltask_class_get(){
        $key = $this->get("key");
        $class_id = $this->get("class_id");
        if($key !== null &&
        $class_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task_class($id, $class_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get tasks' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function alltask_semester_get(){
        $key = $this->get("key");
        $semester_id = $this->get("semester_id");
        if($key !== null &&
        $semester_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task_semester($id, $semester_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get tasks' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function allclass_year_get(){
        $key = $this->get("key");
        $year_id = $this->get("year_id");
        if($key !== null &&
        $year_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task_year($id, $year_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get tasks' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function time_get(){
        $task_id = $this->get("task_id");
        $key = $this->get("key");
        if($key !== null &&
        $task_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task($id, $task_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get task' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }*/

    public function insert_time_post(){
        $key = $this->post("key");
        $class_id = $this->post("class_id");
        $days = $this->post("days");
        $location = $this->post("location");
        $start = $this->post("start");
        $end = $this->post("end");
        
        if($key !== null &&
        $class_id !== null &&
        $days !== null &&
        $location !== null &&
        $start !== null &&
        $end !== null){
            $id = $this->get_id_from_api_key($key);
            if($this->time_model->add_time($id, $class_id, $days, $location, $start, $end)){
                $this->response([
                    'status' => TRUE,
                    'message' => 'Time created sucessfully'
                ], REST_Controller::HTTP_CREATED); //201
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not insert time' 
                ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); //500
            }
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}
