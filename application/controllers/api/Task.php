<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("Task_model", "task_model");
    }

    public function alltask_user_get(){
        $key = $this->get("key");
        if($key !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task_user($id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK); //200
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get tasks' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function alltask_class_get(){
        $key = $this->get("key");
        $class_id = $this->get("class_id");
        if($key !== null &&
        $class_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task_class($id, $class_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get tasks' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function alltask_semester_get(){
        $key = $this->get("key");
        $semester_id = $this->get("semester_id");
        if($key !== null &&
        $semester_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task_semester($id, $semester_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get tasks' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function allclass_year_get(){
        $key = $this->get("key");
        $year_id = $this->get("year_id");
        if($key !== null &&
        $year_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task_year($id, $year_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get tasks' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function task_get(){
        $task_id = $this->get("task_id");
        $key = $this->get("key");
        if($key !== null &&
        $task_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->task_model->get_task($id, $task_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get task' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function insert_task_post(){
        $key = $this->post("key");
        $class_id = $this->post("class_id");
        $name = $this->post("name");
        $deadline = $this->post("deadline");
        $detail = $this->post("detail");
        $img = $this->post("img");
        
        if($key !== null &&
        $class_id !== null &&
        $name !== null &&
        $deadline !== null){
            $id = $this->get_id_from_api_key($key);
            if($this->task_model->add_task($id, $class_id, $name, $deadline, $detail, $img)){
                $this->response([
                    'status' => TRUE,
                    'message' => 'Task created sucessfully'
                ], REST_Controller::HTTP_CREATED); //201
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not insert tasks' 
                ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); //500
            }
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}
