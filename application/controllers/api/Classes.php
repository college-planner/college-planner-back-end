<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("Classes_model", "classes_model");
    }

    public function allclass_user_get(){
        $key = $this->get("key");
        if($key !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->classes_model->get_classes_user($id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get classes' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function allclass_semester_get(){
        $key = $this->get("key");
        $semester_id = $this->get("semester_id");
        if($key !== null &&
        $semester_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->classes_model->get_classes_semester($id, $semester_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get classes' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function allclass_year_get(){
        $key = $this->get("key");
        $year_id = $this->get("year_id");
        if($key !== null &&
        $year_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->classes_model->get_classes_year($id, $year_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK); //200
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get classes' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function class_get(){
        $class_id = $this->get("class_id");
        $key = $this->get("key");
        if($key !== null &&
        $class_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->classes_model->get_class($id, $class_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK); //200
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get class' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function insert_class_post(){
        $key = $this->post("key");
        $semester_id = $this->post("semester_id");
        $name = $this->post("name");
        $detail = $this->post("detail"); //optional
        $color = $this->post("color");
        $credit = $this->post("credit");
        
        if($key !== null &&
        $semester_id !== null &&
        $name !== null &&
        $color !== null &&
        $credit !== null){
            $id = $this->get_id_from_api_key($key);
            if($this->classes_model->add_class($id, $semester_id, $name, $color, $credit, $detail)){
                $this->response([
                    'status' => TRUE,
                    'message' => 'class created sucessfully'
                ], REST_Controller::HTTP_CREATED); //201
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not insert class' 
                ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); //500
            }
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}
