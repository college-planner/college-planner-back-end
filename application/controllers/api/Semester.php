<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Semester extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("Semester_model", "semester_model");
    }

    public function allsemester_user_get(){
        $key = $this->get("key");
        if($key !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->semester_model->get_semesters_user($id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get semester' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function allsemester_year_get(){
        $key = $this->get("key");
        $year_id = $this->get("year_id");
        if($key !== null &&
        $year_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->semester_model->get_semesters_year($id, $year_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get semester' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function semester_get(){
        $semester_id = $this->get("semester_id");
        $key = $this->get("key");
        if($key !== null &&
        $semester_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->semester_model->get_semester($id, $semester_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get semester' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function insert_semester_post(){
        $key = $this->post("key");
        $year_id = $this->post("year_id");
        $start = $this->post("start");
        $end = $this->post("end");
        
        if($key !== null &&
        $start !== null &&
        $end !== null){
            $id = $this->get_id_from_api_key($key);
            if($this->semester_model->add_semester($id, $year_id, $start, $end)){
                $this->response([
                    'status' => TRUE,
                    'message' => 'Semester created sucessfully'
                ], REST_Controller::HTTP_CREATED); //201
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not insert semester' 
                ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); //500
            }
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}
