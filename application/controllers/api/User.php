<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper("get_token");
    }

    public function register_post(){
        $email = $this->post('email');
        $password = $this->post('password');
        $firstname = $this->post('firstname');
        $lastname = $this->post('lastname');
        if( $email !== null && 
            $password !== null &&
            $firstname !== null &&
            $lastname !== null){
            
            $key = get_token();

            if(!$this->user_model->email_exist($email)){
                if($this->user_model->register($email, $password, $firstname, $lastname, $key)){
                    $this->response([
                        'status' => TRUE,
                        'message' => 'User registered sucessfully'
                    ], REST_Controller::HTTP_CREATED); //201
                }
                else{
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Could not register user' 
                    ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); //500
                }
            }
            $this->response([
                'status' => FALSE,
                'message' => 'Email address has already been used' 
            ], REST_Controller::HTTP_OK);
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function login_post(){
        $email = $this->post('email');
        $password = $this->post('password');
        if( $email !== null && 
            $password !== null){
            if($this->user_model->email_exist($email)){
                $user = $this->user_model->login($email, $password);
                if(isset($user->key)){
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Success',
                        'key' => $user->key
                    ], REST_Controller::HTTP_OK); //200
                }
                else{
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Password incorrect' 
                    ], REST_Controller::HTTP_OK); //500
                }
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Email does\'t exist' 
                ], REST_Controller::HTTP_OK); //409
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function user_get(){     //get all user data
		$key = $this->get("key");
        if($key !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->user_model->user($id);
            if($return !== null){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK); //201
            }
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => json_encode($this->input->get())
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function change_email_post(){    //user update
        $old_email = $this->post("old_email");
        $new_email = $this->post("new_email");
        $key = $this->post("key");
        
        if($old_email !== null &&
        $new_email !== null &&
        $key !== null){
            $id = $this->get_id_from_api_key($key);
			if(!$this->user_model->email_exist($new_email)){
				if($this->user_model->update_email($id, $new_email)){
					$this->response([
						'status' => TRUE,
						'message' => 'Email updated sucessfully'
					], REST_Controller::HTTP_OK);
				}
				else{
					$this->response([
						'status' => FALSE,
						'message' => 'Could not update email' 
					], REST_Controller::HTTP_OK);
				}
			}
			else{
				$this->response([
					'status' => FALSE,
					'message' => 'Email address has already been used' 
				], REST_Controller::HTTP_OK);
			}
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
	
	public function change_name_post(){    //user update
        $firstname = $this->post("firstname");
        $lastname = $this->post("lastname");
        $key = $this->post("key");
        
        if($firstname !== null &&
        $lastname !== null &&
        $key !== null){
            $id = $this->get_id_from_api_key($key);
			if($this->user_model->update_name($id, $firstname, $lastname)){
				$this->response([
					'status' => TRUE,
					'message' => 'Name updated sucessfully'
				], REST_Controller::HTTP_OK);
			}
			else{
				$this->response([
					'status' => FALSE,
					'message' => 'Could not update name' 
				], REST_Controller::HTTP_OK);
			}
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
	
	public function change_password_post(){
        $oldpassword = $this->post("oldpassword");
        $newpassword = $this->post("newpassword");
        $key = $this->post("key");
        
        if($oldpassword !== null &&
        $newpassword !== null &&
        $key !== null){
            $id = $this->get_id_from_api_key($key);
			if($this->user_model->check_password($id, $oldpassword)){
				if($this->user_model->update_password($id, $newpassword)){
					$this->response([
						'status' => TRUE,
						'message' => 'Password updated sucessfully'
					], REST_Controller::HTTP_OK);
				}
				else{
					$this->response([
						'status' => FALSE,
						'message' => 'Could not update assword' 
					], REST_Controller::HTTP_OK);
				}
			}
			else{
				$this->response([
					'status' => FALSE,
					'message' => 'Wrong password' 
				], REST_Controller::HTTP_OK);
			}
			
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}
