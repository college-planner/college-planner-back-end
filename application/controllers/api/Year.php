<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Year extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("Year_model", "year_model");
    }
	
	public function year_header_get(){
		$key = $this->get("key");
        if($key !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->year_model->get_year_header($id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get year'
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
	}

    public function allyear_get(){
        $key = $this->get("key");
        if($key !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->year_model->get_years($id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get year' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function year_get(){
        $year_id = $this->get("year_id");
        $key = $this->get("key");
        if($key !== null &&
        $year_id !== null){
            $id = $this->get_id_from_api_key($key);
            $return = $this->year_model->get_year($year_id);
            if($return != false){
                $this->response([
                    'status' => TRUE,
                    'data' => $return
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not get year' 
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function insert_year_post(){    //user update
        $name = $this->post("name");
        $key = $this->post("key");
        
        if($name !== null &&
        $key !== null){
            $id = $this->get_id_from_api_key($key);
            if($this->year_model->add_year($id, $name)){
                $this->response([
                    'status' => TRUE,
                    'message' => 'Year created sucessfully'
                ], REST_Controller::HTTP_CREATED);
            }
            else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Could not insert year' 
                ], REST_Controller::HTTP_OK);
            }
        }
        else{
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Request' 
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}
